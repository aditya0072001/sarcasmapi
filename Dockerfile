FROM python:3.8

RUN pip install virtualenv
ENV VIRTUAL_ENV=/venv
RUN virtualenv venv -p python3
ENV PATH="VIRTUAL_ENV/bin:$PATH"

WORKDIR /app
ADD . /app

RUN pip install -r requirements.txt

# Expose port 
ENV PORT 8080

# Run the application:
CMD exec gunicorn --bind :$PORT --workers 1 --threads 8 app:app