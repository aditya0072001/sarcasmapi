"""
This script runs the application using a development server.
It contains the definition of routes and views for the application.
"""

from flask import Flask ,jsonify
from flask_restful import reqparse, abort, Api, Resource
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
import pickle
import re
app = Flask(__name__)
api = Api(app)

model = tf.keras.models.load_model('sarcasm.h5')
with open('tokenizer.pickle', 'rb') as handle:
    tokenizer = pickle.load(handle)

def preprocess_sentence(w):
    w = str(w).lower().strip()
    w = re.sub(r"([?.!,¿])", r" \1 ", str(w))
    w = re.sub(r'[" "]+', " ", str(w))
    w = re.sub(r"[^a-zA-Z?.!,¿]+", " ", str(w))
    return w

def predict(sentence):
    MAX_LEN = 50
    sentence = preprocess_sentence(sentence)
    sentence = tokenizer.texts_to_sequences([sentence])
    sentence = tf.keras.preprocessing.sequence.pad_sequences(sentence, maxlen = MAX_LEN)
    prediction = model.predict(sentence)
    if prediction[0][0] < 0:
        return jsonify(
	            percentage = 0,
	            value = str(prediction[0][0]),
                isSarcastic = False,
                )
    elif prediction[0][0] > 1.0:
        return jsonify(
	            percentage = 100,
	            value = str(prediction[0][0]),
                isSarcastic = True
                )
    else:
        return jsonify(
	            percentage = str(prediction[0][0]*100),
	            value = str(prediction[0][0]),
                isSarcastic = True
                )
# Make the WSGI interface available at the top level so wfastcgi can get it.
wsgi_app = app.wsgi_app

parser = reqparse.RequestParser()
class Sentence(Resource):
    def post(self,data):
        args = parser.parse_args()
        return predict(data)

api.add_resource(Sentence, '/sentence/<data>')

if __name__ == '__main__':
    import os
    HOST = os.environ.get('SERVER_HOST', 'localhost')
    try:
        PORT = int(os.environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    app.run(HOST, PORT)
